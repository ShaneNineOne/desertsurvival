﻿using System.Collections;
using System.Collections.Generic;
using AC.TimeOfDaySystemFree;
using UnityEngine;

public class DebugManager : MonoBehaviour {

    public bool debugOn;
    private bool showDebugCommands;
    private float oldDayInSeconds;

    private void Update()
    {
        if (debugOn)
        {
            if (Input.GetKeyDown(KeyCode.Tab))
            {
                showDebugCommands = !showDebugCommands;
            }

            if (Input.GetKeyDown(KeyCode.G))
            {
                SurvivalManager.survivalSettings.setEnergy(0);
            }
            if (Input.GetKeyDown(KeyCode.H))
            {
                SurvivalManager.survivalSettings.setEnergy(10);
                SurvivalManager.survivalSettings.setHunger(0);
                SurvivalManager.survivalSettings.setThirst(0);
                SurvivalManager.survivalSettings.setRest(0);
            }
            if (Input.GetKeyDown(KeyCode.J))
            {
                SurvivalManager.survivalSettings.setEnergy(SurvivalManager.survivalSettings.getMaxEnergy());
                SurvivalManager.survivalSettings.addHunger(SurvivalManager.survivalSettings.getMaxHunger());
                SurvivalManager.survivalSettings.addThirst(SurvivalManager.survivalSettings.getMaxThirst());
                SurvivalManager.survivalSettings.addRest(SurvivalManager.survivalSettings.getMaxRest());
            }
            if (Input.GetKeyDown(KeyCode.T))
            {
                oldDayInSeconds = TimeOfDayManager.self.dayInSeconds;
                TimeOfDayManager.self.dayInSeconds = 10;
            }
            if (Input.GetKeyDown(KeyCode.R) && oldDayInSeconds != 0)
            {
                TimeOfDayManager.self.dayInSeconds = oldDayInSeconds;
            }
            if (Input.GetKeyDown(KeyCode.C))
            {
                if (TimeOverlay.self.isShowing()) TimeOverlay.self.hideTimeOverlay();
                else TimeOverlay.self.showTimeOverlay();
            }
            if (Input.GetKeyDown(KeyCode.V))
            {
                SurvivalManager.survivalSettings.toggleSliders();
            }
            if (Input.GetKeyDown(KeyCode.B))
            {
                if (FoodSpawnManager.foodSpawner != null)
                {
                    FoodSpawnManager.foodSpawner.spawnFoods();
                    FoodSpawnManager.foodSpawner.spawnDrinks();
                }
            }

        }
    }

    void OnGUI()
    {
        if (showDebugCommands)
        {
            GUIStyle myStyle = new GUIStyle();
            myStyle.fontSize = 16;
            myStyle.normal.textColor = Color.white;
            GUI.Label(new Rect(Screen.width - 300, 40, 200, 20), "DEBUG COMMANDS:", myStyle);
            GUI.Label(new Rect(Screen.width - 300, 60, 200, 22), "TAB: Toggle Debug Command Display", myStyle);
            GUI.Label(new Rect(Screen.width - 300, 80, 200, 20), "H: Set stats to critical state", myStyle);
            GUI.Label(new Rect(Screen.width - 300, 100, 200, 20), "J: Set stats to maximum values", myStyle);
            GUI.Label(new Rect(Screen.width - 300, 120, 200, 20), "T: Speeds up day/night timescale", myStyle);
            GUI.Label(new Rect(Screen.width - 300, 140, 200, 20), "R: Resets day/night timescale", myStyle);
            GUI.Label(new Rect(Screen.width - 300, 160, 200, 20), "C: Toggle time display", myStyle);
            GUI.Label(new Rect(Screen.width - 300, 180, 200, 20), "V: Toggle stats display", myStyle);
            GUI.Label(new Rect(Screen.width - 300, 200, 200, 20), "B: Respawn food/drink objects", myStyle);
        }
    }

}
