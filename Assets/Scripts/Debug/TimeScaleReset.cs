﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeScaleReset : MonoBehaviour {

	// Use this for initialization
	void Start () {
        if (Time.timeScale == 0) {
            Time.timeScale = 1;
        }
	}
	
	// Update is called once per frame
	void Update () {
        
	}
}
