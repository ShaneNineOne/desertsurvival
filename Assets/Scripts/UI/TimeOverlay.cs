﻿using System.Collections;
using System.Collections.Generic;
using AC.TimeOfDaySystemFree;
using UnityEngine.UI;
using UnityEngine;

public class TimeOverlay : MonoBehaviour {


    public static TimeOverlay self;
    public bool show;
    private TimeOfDayManager dayTime;
    private Text TimeOverlayText;

    private void Start()
    {
        if (self != null)
        {
            Destroy(this.gameObject);
        } else
        {
            self = this;
        }

        dayTime = FindObjectOfType<TimeOfDayManager>();
        TimeOverlayText = GetComponent<Text>();
        TimeOverlayText.gameObject.SetActive(show);
    }

    public bool isShowing()
    {
        return show;
    }

    public void showTimeOverlay()
    {
        TimeOverlayText.gameObject.SetActive(true);
        show = true;
    }

    public void hideTimeOverlay()
    {
        TimeOverlayText.gameObject.SetActive(false);
        show = false;
    }

    private void OnGUI()
    {
        if (show)
        {
            TimeOverlayText.text = dayTime.getFormattedDayTime();
        } 
    }
}
