﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CinematicEffects;
using UnityStandardAssets.Characters.FirstPerson;

public class WakeUpSim : MonoBehaviour {

    public GameObject player;
    public GameObject playerCamera;

    public LensAberrations lens;
    public float vingetteIntencity;
    public float vingetteSmoothness;
    public float vingetteBlur;
    public bool blurOn = false;

    public float blendingSpeed = 0.1f;

 

    void Start() {
        SurvivalManager.survivalSettings.showGUI = false;
        lens = playerCamera.GetComponent<LensAberrations>();

        lens.vignette.intensity = 3;
        lens.vignette.blur = 3;
        lens.vignette.smoothness = 3;

        vingetteIntencity = lens.vignette.intensity;
        vingetteSmoothness = lens.vignette.smoothness;
        vingetteBlur = lens.vignette.blur;

        StartCoroutine(BlurDelay());
        StartCoroutine(Disable());
        StartCoroutine(EnablePlayerController());

        player.GetComponent<FirstPersonController>().enabled = false;
        player.GetComponent<Animator>().enabled = true;

    }

    public float t = 0f;
    public float t2 = 0f;

    void Update() {

        if (vingetteIntencity < 3.1f) {

            t += Time.deltaTime * blendingSpeed;
            vingetteIntencity = Mathf.Lerp(3, 1.24f, t);
        }

        if (vingetteSmoothness < 3.1f) {

            vingetteSmoothness = Mathf.Lerp(3, 0.58f, t);
        }

        if (vingetteBlur < 3.3f && blurOn == true) {

            t2 += Time.deltaTime * blendingSpeed;
            vingetteBlur = Mathf.Lerp(3, 0, t2);
        }


        lens.vignette.intensity = vingetteIntencity;
        lens.vignette.smoothness = vingetteSmoothness;
        lens.vignette.blur = vingetteBlur;

    }

    IEnumerator BlurDelay() {
 
        yield return new WaitForSeconds(2);

        blurOn = true;
 
    }

    IEnumerator Disable() {

        yield return new WaitForSeconds(10);

        this.enabled = false;


    }

    IEnumerator EnablePlayerController() {

        yield return new WaitForSeconds(10);

        player.GetComponent<FirstPersonController>().enabled = true;
        player.GetComponent<Animator>().enabled = false;
        SurvivalManager.survivalSettings.showGUI = true;


    }

}
