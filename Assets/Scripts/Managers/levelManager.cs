﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class levelManager : MonoBehaviour {

    public static levelManager self;

	// Use this for initialization
	void Awake () {
		if (self == null)
        {
            self = this;
            DontDestroyOnLoad(this);
        } else
        {
            Destroy(this.gameObject);
        }
	}
	
    public void LoadScene(string levelName)
    {
        SceneManager.LoadScene(levelName);
    }

    public void returnToTitle()
    {
        this.LoadScene("Title");
    }
}
