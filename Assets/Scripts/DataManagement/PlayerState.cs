﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerState : MonoBehaviour {

    // PUT VALUES TO SAVE HERE
    public float value;
    // PUT VALUES TO SAVE HERE


    // singleton
    public static PlayerState Instance;
    void Awake() {
        if (Instance == null) {
            DontDestroyOnLoad(gameObject);
            Instance = this;
        }
        else if (Instance != this) {
            Destroy(gameObject);
        }
    }


    public PlayerStatistics localPlayerData = new PlayerStatistics();

    //Load data from GlobalControl.
    void Start() {
        value = GlobalControl.Instance.value;
    }

    //Save entire data from to global control   
    public void SavePlayer() {

        // DECLARE SAVED VALUES HERE
        localPlayerData.value = value;
        // DECLARE SAVED VALUES HERE




        GlobalControl.Instance.savedPlayerData = localPlayerData;
    }
}
