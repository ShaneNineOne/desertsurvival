﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// put on player GameObject
public class PlayerDataManagement : MonoBehaviour {

    void Start () {
        if (GlobalControl.Instance.IsSceneBeingLoaded) {
            PlayerState.Instance.localPlayerData = GlobalControl.Instance.LocalCopyOfData;

            GlobalControl.Instance.IsSceneBeingLoaded = false;
        }
    }

	void Update () {

        if (Input.GetKey(KeyCode.F5)) {

            Debug.Log("Saving Data to FILE1");
            PlayerState.Instance.localPlayerData.SceneID = Application.loadedLevel;

            PlayerState.Instance.SavePlayer();
            GlobalControl.Instance.SaveData1();
        }

        if (Input.GetKey(KeyCode.F6)) {

            Debug.Log("Saving Data to FILE2");
            PlayerState.Instance.localPlayerData.SceneID = Application.loadedLevel;

            PlayerState.Instance.SavePlayer();
            GlobalControl.Instance.SaveData2();
        }

        if (Input.GetKey(KeyCode.F7)) {

            Debug.Log("Saving Data to FILE3");
            PlayerState.Instance.localPlayerData.SceneID = Application.loadedLevel;

            PlayerState.Instance.SavePlayer();
            GlobalControl.Instance.SaveData3();
        }

        if (Input.GetKey(KeyCode.F9)) {

            Debug.Log("Loading Data from FILE1 and Reloading scene");
            GlobalControl.Instance.LoadData1();
            GlobalControl.Instance.IsSceneBeingLoaded = true;


            

            int whichScene = GlobalControl.Instance.LocalCopyOfData.SceneID;

            Application.LoadLevel(whichScene);
        }

        if (Input.GetKey(KeyCode.F10)) {

            Debug.Log("Loading Data from FILE2 and Reloading scene");
            GlobalControl.Instance.LoadData2();
            GlobalControl.Instance.IsSceneBeingLoaded = true;




            int whichScene = GlobalControl.Instance.LocalCopyOfData.SceneID;

            Application.LoadLevel(whichScene);
        }

        if (Input.GetKey(KeyCode.F11)) {

            Debug.Log("Loading Data from FILE3 and Reloading scene");
            GlobalControl.Instance.LoadData3();
            GlobalControl.Instance.IsSceneBeingLoaded = true;




            int whichScene = GlobalControl.Instance.LocalCopyOfData.SceneID;

            Application.LoadLevel(whichScene);
        }
    }
}
