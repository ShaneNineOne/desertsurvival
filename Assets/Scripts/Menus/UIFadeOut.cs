﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIFadeOut : MonoBehaviour {



    public Text text;
    public Color newColor;
    public Color newColor2;
    public float oldAlpha;
    public float fadeSpeed;

    public bool active = false;

    void Start() {

        oldAlpha = text.color.a;

        newColor = new Color(text.color.r, text.color.g, text.color.b, 1);

        text.color = newColor;

        newColor2 = new Color(text.color.r, text.color.g, text.color.b, 0);

    }

    void Awake() {

        active = true;

    }

    void Update() {

        if (active == true)
            text.color = Color.Lerp(text.color, newColor2, Time.deltaTime * fadeSpeed);

    }
}