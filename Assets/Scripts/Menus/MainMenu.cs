﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {

    public string startLevel;

    public string instructions;

    public string mainMenu;

    public string credits;

    public int keyDetection = 0;

    public GameObject button1, button2, button3, button4, button5, button6, button7, button8, disableButton1;

    public Image blackScreen;
    public float fadeSpeed;
    public UIFadeOut text1, text2, text3, text4, text5, text6, text7, text8;
    public UIFadeIn ttext1, ttext2, ttext3, ttext4, ttext5, ttext6, ttext7, ttext8;
    public AudioSource titleMusic;
    private int musicFade = 0;
    private bool isNewgame = false;

    public void NewGame() {

        isNewgame = true;
        StartCoroutine(FadeDelay());
        //USE BELOW WHEN INCORPERATING TITLE SCREEN MUSIC
        //Destroy(GameObject.FindWithTag("MusicManager"));
    }

    public void LoadGame() {

        // 1 = title, 2 = newgame, 3 = options, 4 = loadgame, 5 = file1, 6 = file2, 7 = file3, 8 = back;
        // ttext = fade in, text = fade out

        ttext2.enabled = false;
        ttext3.enabled = false;
        ttext4.enabled = false;
        ttext5.enabled = true;
        ttext6.enabled = true;
        ttext7.enabled = true;
        ttext8.enabled = true;

        text2.enabled = true;
        text3.enabled = true;
        text4.enabled = true;
        text5.enabled = false;
        text6.enabled = false;
        text7.enabled = false;
        text8.enabled = false;

        button1.SetActive(false);
        button2.SetActive(false);
        button3.SetActive(false);
        button4.SetActive(false);
        button5.SetActive(true);
        button6.SetActive(true);
        button7.SetActive(true);
        button8.SetActive(true);

        button2.GetComponent<Button>().enabled = false;
        button3.GetComponent<Button>().enabled = false;
        button4.GetComponent<Button>().enabled = false;

        button5.GetComponent<Button>().enabled = true;
        button6.GetComponent<Button>().enabled = true;
        button7.GetComponent<Button>().enabled = true;
        button8.GetComponent<Button>().enabled = true;
    }

    public void BackFromLoad() {

        button1.SetActive(true);
        button2.SetActive(true);
        button3.SetActive(true);
        button4.SetActive(true);

        ttext2.enabled = true;
        ttext3.enabled = true;
        ttext4.enabled = true;
        ttext5.enabled = false;
        ttext6.enabled = false;
        ttext7.enabled = false;
        ttext8.enabled = false;

        text2.enabled = false;
        text3.enabled = false;
        text4.enabled = false;
        text5.enabled = false;
        text6.enabled = true;
        text7.enabled = true;
        text8.enabled = true;

        button1.SetActive(true);
        button2.SetActive(true);
        button3.SetActive(true);
        button4.SetActive(true);
        button5.SetActive(false);
        button6.SetActive(false);
        button7.SetActive(false);
        button8.SetActive(false);

        button2.GetComponent<Button>().enabled = true;
        button3.GetComponent<Button>().enabled = true;
        button4.GetComponent<Button>().enabled = true;

        button5.GetComponent<Button>().enabled = false;
        button6.GetComponent<Button>().enabled = false;
        button7.GetComponent<Button>().enabled = false;
        button8.GetComponent<Button>().enabled = false;

    }


    public void Instructions() {
        levelManager.self.LoadScene(instructions);
    }

    public void Menu() {
        levelManager.self.LoadScene(mainMenu);
    }



    public void File1() {

        Debug.Log("Loading Data from FILE1 and starting game");
        GlobalControl.Instance.LoadData1();
        GlobalControl.Instance.IsSceneBeingLoaded = true;

        StartCoroutine(FadeDelay());

    }

    public void File2() {

        Debug.Log("Loading Data from FILE2 and starting game");
        GlobalControl.Instance.LoadData2();
        GlobalControl.Instance.IsSceneBeingLoaded = true;

        StartCoroutine(FadeDelay());

    }

    public void File3() {

        Debug.Log("Loading Data from FILE3 and starting game");
        GlobalControl.Instance.LoadData3();
        GlobalControl.Instance.IsSceneBeingLoaded = true;

        StartCoroutine(FadeDelay());

    }

    IEnumerator FadeDelay() {
 
            musicFade = 1;

            ttext1.enabled = false;
            ttext2.enabled = false;
            ttext3.enabled = false;
            ttext4.enabled = false;
            ttext5.enabled = false;
            ttext6.enabled = false;
            ttext7.enabled = false;
            ttext8.enabled = false;

            text1.enabled = true;
            text2.enabled = true;
            text3.enabled = true;
            text4.enabled = true;
            text5.enabled = true;
            text6.enabled = true;
            text7.enabled = true;
            text8.enabled = true;

            button2.GetComponent<Button>().enabled = false;
            button3.GetComponent<Button>().enabled = false;
            button4.GetComponent<Button>().enabled = false;
            button5.GetComponent<Button>().enabled = false;
            button6.GetComponent<Button>().enabled = false;
            button7.GetComponent<Button>().enabled = false;
            button8.GetComponent<Button>().enabled = false;

            float alpha = blackScreen.color.a;
            for (float t = 0.0f; t < 1.0f; t += Time.deltaTime * fadeSpeed / 1) {
                Color newColor = new Color(1, 1, 1, Mathf.Lerp(alpha, 1, t));
                blackScreen.color = newColor;

                yield return null;
            }

            yield return new WaitForSeconds(5);

            if (isNewgame == true) {
                levelManager.self.LoadScene(startLevel);
            }
            else {
                levelManager.self.LoadScene("Main");
            }

    }

    void Update() {

        if (Input.anyKey)
        keyDetection = 1;

        if (keyDetection == 1) {

            button1.SetActive(true);
            button2.SetActive(true);
            button3.SetActive(true);
            button4.SetActive(true);

            disableButton1.SetActive(false);


        }

        if (musicFade == 1) {
            titleMusic.volume -= 0.0004f;
        }
    }



}
