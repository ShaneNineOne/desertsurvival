﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseScreen : MonoBehaviour {

    // file1, file2, file3
    public GameObject button1, button2, button3;
    bool clicked = true;

    public void SaveGame() {

        if (clicked == true) {
            clicked = false;
            button1.SetActive(true);
            button2.SetActive(true);
            button3.SetActive(true);
        } else {
            clicked = true;
            button1.SetActive(false);
            button2.SetActive(false);
            button3.SetActive(false);
        }

    }

    public void SaveFile1() {

        Debug.Log("Saving Data to FILE1");
        PlayerState.Instance.localPlayerData.SceneID = Application.loadedLevel;

        PlayerState.Instance.SavePlayer();
        GlobalControl.Instance.SaveData1();

    }

    public void SaveFile2() {

        Debug.Log("Saving Data to FILE2");
        PlayerState.Instance.localPlayerData.SceneID = Application.loadedLevel;

        PlayerState.Instance.SavePlayer();
        GlobalControl.Instance.SaveData2();

    }

    public void SaveFile3() {

        Debug.Log("Saving Data to FILE3");
        PlayerState.Instance.localPlayerData.SceneID = Application.loadedLevel;

        PlayerState.Instance.SavePlayer();
        GlobalControl.Instance.SaveData3();

    }

}
