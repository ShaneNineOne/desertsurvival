﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextFade : MonoBehaviour {

    public Text text;
    public int fadeStartInSeconds;
    float myAlpha;
    private bool fade = false;

    void Start() {
        myAlpha = text.color.a;
        StartCoroutine(Fade());
    }

    void Update() {

        if (fade == true) {
            myAlpha -= 0.01f;
            text.color = new Color(1, 1, 1, myAlpha);
        } 
    }

    IEnumerator Fade() {
        yield return new WaitForSeconds(fadeStartInSeconds);
        fade = true;
    }

}
