﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleScreens : MonoBehaviour {

    public int sceneNum;

    void Start() {

        StartCoroutine(Begin());
    }

    IEnumerator Begin() {

        if (sceneNum == 1) {

            Debug.Log("intro 1 started");
            yield return new WaitForSeconds(8);

            Application.LoadLevel("Intro2");

        }

        if (sceneNum == 2) {

            Debug.Log("intro 2 started");
            yield return new WaitForSeconds(16);

            Application.LoadLevel("Intro3");

        }

        if (sceneNum == 3) {

            Debug.Log("intro 3 started");
            yield return new WaitForSeconds(15);

            Application.LoadLevel("Intro4");

        }

        if (sceneNum == 4) {

            Debug.Log("intro 4 started");
            yield return new WaitForSeconds(14);

            Application.LoadLevel("Intro5");

        }

        if (sceneNum == 5) {

            Debug.Log("intro 5 started");
            yield return new WaitForSeconds(10);

            Application.LoadLevel("Main");

        }

        





    }
}