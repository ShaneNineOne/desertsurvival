﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TextTyper : MonoBehaviour {

    public float letterPause = 0.2f;
    public AudioSource typeSound1;
    public AudioSource typeSound2;


    string message;
    Text textComp;

    public float randomNum;

    // Use this for initialization
    void Start() {
        textComp = GetComponent<Text>();
        message = textComp.text;
        textComp.text = "";
        StartCoroutine(TypeText());
    }

    IEnumerator TypeText() {
        foreach (char letter in message.ToCharArray()) {
            textComp.text += letter;

            randomNum = Random.Range(-1, 1);

            letterPause = Random.Range(0.07f, 0.2f);

            if (randomNum < 0) {
                typeSound1.pitch = Random.Range(0.9f, 1.10f);
                typeSound1.volume = Random.Range(0.8f, 1f);
                typeSound1.Play();
            } else {
                typeSound2.pitch = Random.Range(0.9f, 1.10f);
                typeSound2.volume = Random.Range(0.8f, 1f);
                typeSound2.Play();
            }



            yield return 0;
            yield return new WaitForSeconds(letterPause);
        }
    }
}