﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scene1 : MonoBehaviour {

    public Camera camera;

    public GameObject startPosition;
    public GameObject endPosition;

    public bool cameraPan = false;

    void Start() {
        StartCoroutine(Scene());
    }

    IEnumerator Scene() {

        Cam1();

        yield return new WaitForSeconds(5);
        
    }

    void Cam1 () {



        startPosition.transform.position = new Vector3(303f, 49f, 50f);
        endPosition.transform.position = new Vector3(212f, 49f, 50f);

        camera.transform.position = startPosition.transform.position;
        camera.transform.rotation = Quaternion.Euler(-6f, 10f, 0f);

        cameraPan = true;

    }


    void Update() {

        if (cameraPan == true) {
            camera.GetComponent<CameraPan>().enabled = true;
        } else {
            camera.GetComponent<CameraPan>().enabled = false;
        }


    }
}
