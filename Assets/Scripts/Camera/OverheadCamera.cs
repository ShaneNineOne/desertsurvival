﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverheadCamera : MonoBehaviour {

    public static OverheadCamera overheadCameraManager;

    /// <summary>
    /// Where was the main camera located before we moved to an overhead camera point?
    /// </summary>
    private Vector3 oldCameraPosition;
    /// <summary>
    /// What was the main camera's rotation before we moved to an overhead camera point?
    /// </summary>
    private Quaternion oldCameraRotation;
    /// <summary>
    /// The list of all available cameras we can choose from.
    /// </summary>
    [SerializeField] private List<Transform> cameraPositions;
    /// <summary>
    /// The index of the overhead camera we have chosen to use.
    /// </summary>
    [SerializeField] private int selectedCameraIndex;

	void Start () {
        if (overheadCameraManager == null)
        {
            overheadCameraManager = this;
        } else
        {
            Destroy(this.gameObject);
        }

		foreach (GameObject target in GameObject.FindGameObjectsWithTag("OverheadCam"))
        {
            cameraPositions.Add(target.transform);
        }
    }

    public void enableOverheadCamera()
    {
        oldCameraPosition = Camera.main.transform.position;
        oldCameraRotation = Camera.main.transform.rotation;
        selectOverheadCamera();
        Camera.main.transform.position = cameraPositions[selectedCameraIndex].position;
        Camera.main.transform.rotation = cameraPositions[selectedCameraIndex].rotation;
    }

    public void disableOverheadCamera()
    {
        Camera.main.transform.position = oldCameraPosition;
        Camera.main.transform.rotation = oldCameraRotation;
    }

    private void selectOverheadCamera()
    {
        selectedCameraIndex = Random.Range(0, cameraPositions.Count);
    }


}
