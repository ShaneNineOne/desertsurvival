﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class interactableObject : MonoBehaviour {

    /// <summary>
    /// The name of the object.
    /// </summary>
    [SerializeField] private string objectName;
    /// <summary>
    /// The text to be displayed when examining an object.
    /// </summary>
    [SerializeField] private string objectText;
    /// <summary>
    /// What type is the interactableObject?
    /// </summary>
    [SerializeField] private int objectType;
    /// <summary>
    /// When was the interactableObject last used?
    /// </summary>
    [SerializeField] float lastUse = 0f;
    /// <summary>
    /// How many seconds does it take before we can reuse the interactableObject?
    /// </summary>
    [SerializeField] float cooldownTime;
    /// <summary>
    /// Should the object be destroyed after it is used?
    /// </summary>
    [SerializeField] bool destroyOnUse;

    //NOTE: Object type is a unique key that identifys what kind of object we are dealing with for handling...
    //type 0: Text object (Player can "examine" and view a text box containing objectText)
    private void Start()
    {
        //THIS IS CRUCIAL!!! Strings entered in the editor, for some dumb reason, automatically escape out of new lines. We need this to fix \n functionality.
        objectText = objectText.Replace("\\n", "\n");
        lastUse = -cooldownTime;//We need to set lastUse to the opposite of our cooldown timer at the start, else we need to wait until the cooldown timer has passed to eat anything.
    }

    public int getObjectType()
    {
        return objectType;
    }

    public string getObjectText()
    {
        return objectText;
    }

    public string getObjectName()
    {
        return objectName;
    }

    public void setLastUse(float time)
    {
        lastUse = time;
    }

    public float getLastUse()
    {
        return lastUse;
    }

    public float getCooldownTime()
    {
        return cooldownTime;
    }

    public bool getDestroyOnUse()
    {
        return destroyOnUse;
    }
}
