﻿using System.Collections;
using System.Collections.Generic;
using AC.TimeOfDaySystemFree;
using UnityEngine;

public class FoodSpawnManager : MonoBehaviour
{

    public static FoodSpawnManager foodSpawner;
    [SerializeField]
    private List<Transform> foodTargets;
    [SerializeField]
    private List<Transform> drinkTargets;
    [SerializeField]
    private int totalNumOfFoods;
    [SerializeField]
    private int totalNumOfDrinks;
    [SerializeField]
    private Transform[] availableFoods;
    [SerializeField]
    private Transform[] availableDrinks;
    [SerializeField]
    private Transform foodObjectStorage;
    [SerializeField]
    private Transform drinkObjectStorage;
    public Terrain gameTerrain;

    /// <summary>
    /// The current number of foods that have been spawned. BE CAREFUL MODIFYING THIS - ONLY DO SO WHEN REMOVING OR ADDING A FOOD ITEM THAT'S BEEN SPAWNED.
    /// </summary>
    public int currentNumOfFoods;
    /// <summary>
    /// The current number of drinks that have been spawned. BE CAREFUL MODIFYING THIS - ONLY DO SO WHEN REMOVING OR ADDING A DRINK ITEM THAT'S BEEN SPAWNED.
    /// </summary>
    public int currentNumOfDrinks;

    private void Awake()
    {
        if (foodSpawner == null) foodSpawner = this;
        else Destroy(this.gameObject);
    }

        // Use this for initialization
    void Start()
    {
        //Handling to ensure we don't set a total above the available number of spawn points.
        foreach (GameObject target in GameObject.FindGameObjectsWithTag("FoodSpawn"))
        {
            foodTargets.Add(target.transform);
        }
        foreach (GameObject target in GameObject.FindGameObjectsWithTag("DrinkSpawn"))
        {
            drinkTargets.Add(target.transform);
        }
        if (totalNumOfFoods > foodTargets.Count) totalNumOfFoods = foodTargets.Count;
        if (totalNumOfDrinks > drinkTargets.Count) totalNumOfDrinks = drinkTargets.Count;
        currentNumOfDrinks = 0;
        currentNumOfFoods = 0;
        spawnFoods();
        spawnDrinks();
    }

    public void spawnFoods()
    {
        if (currentNumOfFoods == totalNumOfFoods) return;
        for (int i = currentNumOfFoods; i < totalNumOfFoods; i++)
        {
            bool spawnCleared = false;
            int chosenTarget = Random.Range(0, (foodTargets.Count - 1));
            int currentTarget = chosenTarget;
            //NOTE: THIS IS PURELY TO PREVENT POTENTIAL INFINITE LOOPS WHILE DEBUGGING THIS. DELETE THIS COUNTER FOR FINAL GAME ONCE LOGIC IS SOUND.
            int DEBUGCOUNTER = 0;

            while (!spawnCleared)
            {
                Vector3 spawnLocation = foodTargets[currentTarget].transform.position;

                //Assume the spawn point is cleared.
                spawnCleared = true;
                RaycastHit hit;
                if (Physics.Raycast(spawnLocation, Vector3.down, out hit, 1 << 9))
                {
                    if (hit.collider.CompareTag("interactableObject"))
                    {
                        //Update our knowledge if the spawn actually is not clear.
                        spawnCleared = false;
                    }
                }

                if (!spawnCleared)
                {
                    currentTarget++;
                    if (currentTarget >= foodTargets.Count)
                    {
                        currentTarget = 0;
                    }
                    if (currentTarget == chosenTarget) break;//Something went wrong - nothing free...
                }
                else
                {
                    //A math note: We have to add half the spawned object's height when spawning it, otherwise it starts off halfway in the ground. From there, we add the manually set object height to bury it in the ground the appropriate amount.
                    currentNumOfFoods++;
                    foodObject targetFood = availableFoods[Random.Range(0, availableFoods.Length)].GetComponent<foodObject>();
                    Transform newFood = Instantiate(targetFood.transform, new Vector3(spawnLocation.x, gameTerrain.SampleHeight(spawnLocation) + gameTerrain.transform.position.y + (targetFood.transform.GetComponent<Renderer>().bounds.size.y / 2) + targetFood.getSpawnHeight(), spawnLocation.z), Quaternion.Euler(0.0f, Random.Range(0.0f, 360.0f), 0.0f));
                    newFood.GetComponent<foodObject>().setWasSpawned(true);
                    //Manually set to ensure accuracy...
                    newFood.tag = "interactableObject";
                    newFood.gameObject.layer = 9;
                    newFood.SetParent(foodObjectStorage);
                    break;
                }
            }


            if (DEBUGCOUNTER >= 100)
            {
                Debug.Log("ERROR: Had to break the loop!");
                break;
            }
            DEBUGCOUNTER++;
        }
    }


    public void spawnDrinks()
    {
        if (currentNumOfDrinks == totalNumOfDrinks) return;
        for (int i = currentNumOfDrinks; i < totalNumOfDrinks; i++)
        {
            bool spawnCleared = false;
            int chosenTarget = Random.Range(0, (drinkTargets.Count - 1));
            int currentTarget = chosenTarget;
            //NOTE: THIS IS PURELY TO PREVENT POTENTIAL INFINITE LOOPS WHILE DEBUGGING THIS. DELETE THIS COUNTER FOR FINAL GAME ONCE LOGIC IS SOUND.
            int DEBUGCOUNTER = 0;

            while (!spawnCleared)
            {
                Vector3 spawnLocation = drinkTargets[currentTarget].transform.position;
                //Assume the spawn point is cleared.
                spawnCleared = true;

                RaycastHit hit;
                if (Physics.Raycast(spawnLocation, Vector3.down, out hit, 1 << 9))
                {
                    if (hit.collider.CompareTag("interactableObject"))
                    {
                        //Update our knowledge if the spawn actually is not clear.
                        spawnCleared = false;
                    }
                }

                if (!spawnCleared)
                {
                    currentTarget++;
                    if (currentTarget >= drinkTargets.Count)
                    {
                        currentTarget = 0;
                    }
                    if (currentTarget == chosenTarget) break;//Something went wrong - nothing free...
                }
                else
                {
                    currentNumOfDrinks++;
                    foodObject targetDrink = availableDrinks[Random.Range(0, availableDrinks.Length)].GetComponent<foodObject>();
                    Transform newDrink = Instantiate(targetDrink.transform, new Vector3(spawnLocation.x, Terrain.activeTerrain.SampleHeight(spawnLocation) + Terrain.activeTerrain.transform.position.y + (targetDrink.transform.GetComponent<Renderer>().bounds.size.y / 2) + targetDrink.getSpawnHeight(), spawnLocation.z), Quaternion.Euler(0.0f, Random.Range(0.0f, 360.0f), 0.0f));
                    newDrink.GetComponent<foodObject>().setWasSpawned(true);
                    newDrink.SetParent(drinkObjectStorage);
                    break;
                }
            }


            if (DEBUGCOUNTER >= 100)
            {
                Debug.Log("ERROR: Had to break the loop!");
                break;
            }
            DEBUGCOUNTER++;
        }
    }
}
