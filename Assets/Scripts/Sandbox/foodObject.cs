﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class foodObject : interactableObject {

    [SerializeField] private float amountRestored;
    [SerializeField] private float spawnHeight;
    [SerializeField] private bool wasSpawned;

	public float getAmountRestored()
    {
        return amountRestored;
    }

    public bool getWasSpawned()
    {
        return wasSpawned;
    }

    public float getSpawnHeight()
    {
        return spawnHeight;
    }

    public void setWasSpawned(bool setTo)
    {
        wasSpawned = setTo;
    }
}
