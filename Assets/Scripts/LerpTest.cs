﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LerpTest : MonoBehaviour {

    public float valueToLerp;
    public float t = 0f;
    public float speed = 0.3f;

	void Update () {

        if (valueToLerp < 1) {

            t += Time.deltaTime * speed;
            valueToLerp = Mathf.Lerp(0, 1, t);
           

        }

    }


}
