﻿using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.CinematicEffects;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine;
using UnityEngine.UI;



public class PlayerVisualController : MonoBehaviour {

    public static PlayerVisualController playerVisual;

    [SerializeField] LensAberrations lens;

    public bool defaultActive;
    [SerializeField] private float defaultBlur;
    [SerializeField] private float defaultIntensity;
    [SerializeField] private float defaultSmoothness;
    [SerializeField] private Color defaultColor;

    public bool nearDeathActive;
    [SerializeField] private float nearDeathBlur;
    [SerializeField] private float nearDeathIntensity;
    [SerializeField] private float nearDeathSmoothness;
    [SerializeField] private Color nearDeathColor;

    public bool deathActive;
    [SerializeField] private float deathBlur;
    [SerializeField] private float deathIntensity;
    [SerializeField] private float deathSmoothness;
    [SerializeField] private Color deathColor;
    [SerializeField] private Image deathImage;
    [SerializeField] private Text deathText;
    [SerializeField] private Button deathButton;
    [SerializeField] private Image deathButtonImage;
    [SerializeField] private Text deathButtonText;

    private float startTime;
    private float startBlur;
    private float startIntensity;
    private float startSmoothness;
    private Color startColor;
    private float blendingSpeed = 0.3f;

    private const float defaultDistortion = 0;
    private const float maxDistortion = 75;
    [SerializeField] float startDistortion;

    [SerializeField] private bool nearDeathFading;
    [SerializeField] private bool deathFading;
    [SerializeField] private bool defaultFading;

    public bool forceEffects;

    private void Start()
    {
        if (playerVisual == null) playerVisual = this;
        else Destroy(this.gameObject);
        defaultActive = SurvivalManager.survivalSettings.getEnergy() > 10;
        nearDeathActive = !defaultActive;
    }

    private void Update()
    {
        if (forceEffects) return;
        if (nearDeathActive) renderDistortion();
        if (nearDeathFading) enableNearDeathEffects();
        if (deathFading) enableDeathEffects();
        if (defaultFading) enableDefaultEffects();
    }

    /// <summary>
    /// Render the appropriate amount of distortion, per our current energy value.
    /// </summary>
    private void renderDistortion()
    {
        if (SurvivalManager.survivalSettings.getEnergy() > 10) return;

        //Max distortion / low energy threshold * (low energy threshold - currentEnergy) = distortion amount proportional to current energy.
        lens.distortion.amount = (maxDistortion / 10) * (10 - SurvivalManager.survivalSettings.getEnergy());
    }

    /// <summary>
    /// Endable and render the near death screen effects, with a fade in transition from the previous effects.
    /// </summary>
    private void enableNearDeathEffects()
    {
        if (!nearDeathFading) return;
        if (lens.vignette.intensity == nearDeathIntensity && lens.vignette.blur == nearDeathBlur && lens.vignette.smoothness == nearDeathSmoothness && lens.vignette.color == nearDeathColor)
        {
            nearDeathFading = false;
            return;
        }
        startTime += Time.deltaTime * blendingSpeed;
        if (lens.vignette.intensity != nearDeathIntensity) lens.vignette.intensity = Mathf.Lerp(startIntensity, nearDeathIntensity, startTime);
        if (lens.vignette.smoothness != nearDeathSmoothness) lens.vignette.smoothness = Mathf.Lerp(startSmoothness, nearDeathSmoothness, startTime);
        if (lens.vignette.blur != nearDeathBlur) lens.vignette.blur = Mathf.Lerp(startBlur, nearDeathBlur, startTime);
        if (lens.vignette.color != nearDeathColor) lens.vignette.color = Color.Lerp(startColor, nearDeathColor, startTime);
    }

    public void enableDeathEffects()
    {
        if (!deathFading) return;
        if (lens.vignette.intensity == deathIntensity && lens.vignette.blur == deathBlur && lens.vignette.smoothness == deathSmoothness && lens.vignette.color == deathColor && deathImage.color.a == 1 && deathText.color.a == 1 && deathButtonImage.color.a == 1 && deathButtonText.color.a == 1)
        {
            deathFading = false;
            deathButton.interactable = true;
            return;
        }
        startTime += Time.deltaTime * blendingSpeed;
        if (lens.vignette.intensity != deathIntensity) lens.vignette.intensity = Mathf.Lerp(startIntensity, deathIntensity, startTime);
        if (lens.vignette.smoothness != deathSmoothness) lens.vignette.smoothness = Mathf.Lerp(startSmoothness, deathSmoothness, startTime);
        if (lens.vignette.blur != deathBlur) lens.vignette.blur = Mathf.Lerp(startBlur, deathBlur, startTime);
        if (lens.vignette.color != deathColor) lens.vignette.color = Color.Lerp(startColor, deathColor, startTime);
        float alpha = (Mathf.Lerp(0, 1, startTime));
        if (deathImage.color.a != 255) deathImage.color = new Color(deathImage.color.r, deathImage.color.g, deathImage.color.b, alpha);
        if (deathText.color.a != 255) deathText.color = new Color(deathText.color.r, deathText.color.g, deathText.color.b, alpha);
        if (deathButtonImage.color.a != 255) deathButtonImage.color = new Color(deathButtonImage.color.r, deathButtonImage.color.g, deathButtonImage.color.b, alpha);
        if (deathButtonText.color.a != 255) deathButtonText.color = new Color(deathButtonText.color.r, deathButtonText.color.g, deathButtonText.color.b, alpha);
        ;
    }

    /// <summary>
    /// Enable and render the default screen effects, with a fade in transition from the previous effects.
    /// </summary>
    private void enableDefaultEffects()
    {
        if (!defaultFading) return;
        if (lens.vignette.intensity == defaultIntensity && lens.vignette.blur == defaultBlur && lens.vignette.smoothness == defaultSmoothness && lens.vignette.color == defaultColor && lens.distortion.amount == defaultDistortion)
        {
            defaultFading = false;
            return;
        }
        startTime += Time.deltaTime * blendingSpeed;
        if (lens.vignette.intensity != defaultIntensity) lens.vignette.intensity = Mathf.Lerp(startIntensity, defaultIntensity, startTime);
        if (lens.vignette.smoothness != defaultSmoothness) lens.vignette.smoothness = Mathf.Lerp(startSmoothness, defaultSmoothness, startTime);
        if (lens.vignette.blur != defaultBlur) lens.vignette.blur = Mathf.Lerp(startBlur, defaultBlur, startTime);
        if (lens.vignette.color != defaultColor) lens.vignette.color = Color.Lerp(startColor, defaultColor, startTime);
        if (lens.distortion.amount != defaultDistortion) lens.distortion.amount = Mathf.Lerp(startDistortion, defaultDistortion, startTime);
    }

    /// <summary>
    /// Set values for the near death fade
    /// </summary>
    private void setNearDeathFade()
    {
        if (nearDeathActive) return;
        nearDeathActive = true;
        defaultActive = false;
        setFade();
        nearDeathFading = true;
        defaultFading = false;
    }

    /// <summary>
    /// Set values for the default fade
    /// </summary>
    private void setDefaultFade()
    {
        if (defaultActive) return;
        defaultActive = true;
        nearDeathActive = false;
        setFade();
        defaultFading = true;
        nearDeathFading = false;
    }

    public void setDeathFade()
    {
        deathText.text = SurvivalManager.survivalSettings.getSurvivalTime();
        defaultActive = false;
        nearDeathActive = false;
        setFade();
        deathFading = true;
        deathImage.gameObject.SetActive(true);
    }

    /// <summary>
    /// Sets the current state to default, without preparing for a fade.
    /// </summary>
    private void setDefaultState()
    {
        defaultActive = true;
        nearDeathActive = false;
        defaultFading = false;
        nearDeathFading = false;
    }

    /// <summary>
    /// Sets the currents state to near death, without preparing for a fade.
    /// </summary>
    private void setNearDeathState()
    {
        defaultActive = false;
        nearDeathActive = true;
        defaultFading = false;
        nearDeathFading = false;
    }

    /// <summary>
    /// Set the starting values for general use in each fade.
    /// </summary>
    private void setFade()
    {
        startTime = 0;
        startSmoothness = lens.vignette.smoothness;
        startIntensity = lens.vignette.intensity;
        startBlur = lens.vignette.blur;
        startColor = lens.vignette.color;
        startDistortion = lens.distortion.amount;
    }

    /// <summary>
    /// Enable default effects immediately, with no fade
    /// </summary>    
    public void forceDefaultEffects()
    {
        lens.vignette.blur = defaultBlur;
        lens.vignette.intensity = defaultIntensity;
        lens.vignette.smoothness = defaultSmoothness;
        lens.vignette.color = defaultColor;
        lens.distortion.amount = defaultDistortion;
    }

    /// <summary>
    /// Enable near death effects immediately, with no fade
    /// </summary>
    public void forceNearDeathEffects()
    {
        lens.vignette.blur = nearDeathBlur;
        lens.vignette.intensity = nearDeathIntensity;
        lens.vignette.smoothness = nearDeathSmoothness;
        lens.vignette.color = nearDeathColor;
        renderDistortion();
    }

    /// <summary>
    /// Determines, based on survival stats, which current effect should be displayed and sets it up for a fade.
    /// </summary>
    public void setCurrentFade()
    {
        if (SurvivalManager.survivalSettings.getEnergy() > 10) setDefaultFade();
        else if (SurvivalManager.survivalSettings.getEnergy() > 0) setNearDeathFade();
        else setDeathFade();
    }

    /// <summary>
    /// Determines, based on survival stats, which current effect should be displayed.
    /// </summary>
    public void setCurrentState()
    {
        if (SurvivalManager.survivalSettings.getEnergy() > 10) setDefaultState();
        else if (SurvivalManager.survivalSettings.getEnergy() > 0) setNearDeathState();
        else setNearDeathState();
    }

    /// <summary>
    /// Enable whatever effects should be active at this moment immediately, with no fade
    /// </summary>
    public void forceCurrentEffects()
    {
        if (nearDeathActive) forceNearDeathEffects();
        else forceDefaultEffects();
    }
}
