﻿using System.Collections;
using System.Collections.Generic;
using AC.TimeOfDaySystemFree;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.UI;
using UnityEngine;

public class SurvivalManager : MonoBehaviour {
    public static SurvivalManager survivalSettings;

    public float totalSurvivalTime;

    private TimeOfDayManager dayTime;
    private FirstPersonController playerControl;
    [SerializeField] private float globalTimer;
    /// <summary>
    /// How many seconds do we perform depletions on hunger/thirst or adjust warmth naturally?
    /// </summary>
    private const float interval = 1;
    /// <summary>
    /// What is the next time we will perform depletions on hunger/thirst or adjust warmth naturally?
    /// </summary>
    private float nextTime = 0;

    [SerializeField] private float hunger;
    [SerializeField] private float maxHunger;
    [SerializeField] private float hungerSurvivalTime;
	[SerializeField] private Slider hungerSlider;
    [SerializeField] private Image hungerImage;

    [SerializeField] private float thirst;
    [SerializeField] private float maxThirst;
    [SerializeField] private float thirstSurvivalTime;
	[SerializeField] private Slider thirstSlider;
    [SerializeField] private Image thirstImage;

    [SerializeField] private float rest;
    [SerializeField] private float maxRest; 
    [SerializeField] private float restSurvivalTime;
	[SerializeField] private Slider fatigueSlider;
    [SerializeField] private Image fatigueImage;

    [SerializeField] private float energy;
    [SerializeField] private float maxEnergy;
    [SerializeField] private float energySurvivalTime;
    [SerializeField] private Slider energySlider;
    [SerializeField] private Image energyImage;

    public bool showGUI;

    //How long has hungr/thirst been at 0 since we last slept?
    private float hungerOverflowSeconds;
    private float thirstOverflowSeconds;

    /// <summary>
    /// Should we decay hunger and thirst, or leave them alone?
    /// </summary>
    public bool enableDecay;

    private void Awake()
    {
        if (survivalSettings == null)
            survivalSettings = this;
        else
            Destroy(this.gameObject);
    }

    // Use this for initialization
    void Start () {
        dayTime = FindObjectOfType<TimeOfDayManager>();
        playerControl = FindObjectOfType<FirstPersonController>();
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        //Some math notes... maxHunger / hungerSurvivalTime gives us a nice ratio that we can decrement by each second that will, in hungerSurvivalTime seconds, deplete hunger to 0 from our maximum hunger.
        //This is a fixed ratio, but there would be ways to maybe slow down this rate if we wanted to get into some other mechanics in the future.

        //Important: dont decay anything if it's not enabled. This will lead to more problems than it's worth.
        if (!enableDecay) return;

        if (energy <= 0)
        {
            playerControl.showCursor();
            playerInteractor.self.enableInteractiveUI = false;
            playerInteractor.self.hideInteractText();
            playerControl.setCanMove(false);
            this.enableDecay = false;
            this.toggleSliders();
            PlayerVisualController.playerVisual.setDeathFade();
        }
            
        globalTimer += Time.fixedDeltaTime;
        if (globalTimer >= nextTime)
        {
            nextTime = globalTimer + interval;
            //A NOTE ON MATH: The interval is here to just keep us from evaluating all of this every single frame... we instead do it every (interval) seconds.
            //Since the interval is not always going to be every second (we may decide to make it every 2 seconds, for example), we need to multiply the depletion amount by interval.
            //Otherwise, we would always be decrementing as if we had passed by a single second, even if we actually just went by 2 seconds. This would thereby throw off our timescale for depletion - not good!

            //Deplete hunger. TODO: If you're moving, multiply maxHunger / hungerSurvivalTime by some movement ratio (1.3f, etc.)
            hunger = hunger - ((maxHunger / hungerSurvivalTime) * interval);
			hungerSlider.value = hunger;
            if (hunger < 0) hunger = 0;

            //Deplete thirst
            thirst = thirst - ((maxThirst / thirstSurvivalTime) * interval);
			thirstSlider.value = thirst;
            if (thirst < 0) thirst = 0;

            rest = rest - ((maxRest / restSurvivalTime) * interval);
			fatigueSlider.value = rest;
            if (rest < 0) rest = 0;

            if (hunger == 0 || thirst == 0 || rest == 0)
            {
                //TODO: Multipliers that increase depletion rate with more bad survival cases active(ie. 50% faster if your hunger and thirst are 0 and you're freezing or overheated)
                energy = energy - ((maxEnergy / energySurvivalTime) * interval);
				energySlider.value = energy;
                if (energy < 0) energy = 0; 
            }

            PlayerVisualController.playerVisual.setCurrentFade();
        }
    }

    public void addHunger(float amount)
    {
        hunger += amount;
        if (hunger > maxHunger) hunger = maxHunger;

		hungerSlider.value = hunger;
    }

    public void addThirst(float amount)
    {
        thirst += amount;
        if (thirst > maxThirst) thirst = maxThirst;

		thirstSlider.value = thirst;
    }

    public void addRest(float amount)
    {
        rest += amount;
        if (rest > maxRest) rest = maxRest;

		fatigueSlider.value = rest;
    }

    public float getHunger()
    {
        return hunger;
    }

    public float getThirst()
    {
        return thirst;
    }

    public float getRest()
    {
        return rest;
    }

    public float getEnergy()
    {
        return energy;
    }

    public void setHunger(float value)
    {
        hunger = value;
    }

    public void setThirst(float value)
    {
        thirst = value;
    }

    public void setRest(float value)
    {
        rest = value;
    }

    public void setEnergy(float value)
    {
        energy = value;
    }

    public float getMaxHunger()
    {
        return maxHunger;
    }

    public float getMaxThirst()
    {
        return maxThirst;
    }

    public float getMaxRest()
    {
        return maxRest;
    }

    public float getMaxEnergy()
    {
        return maxEnergy;
    }

    public void restRegenerationForHours(float numOfHours)
    {
        float realWorldTimescale = dayTime.getRealWorldTimescale();
        float secondsToAdvance = numOfHours * realWorldTimescale;
        //Recover rest at half the rate it depletes.
        rest = rest + ((maxRest / restSurvivalTime * 1.5f) * secondsToAdvance);
        if (rest > maxRest) rest = maxRest;
    }

    public void restDepletionForHours(float numOfHours)
    {
        float realWorldTimescale = dayTime.getRealWorldTimescale();
        float secondsToAdvance = numOfHours * realWorldTimescale;
        rest = rest - ((maxRest / restSurvivalTime) * secondsToAdvance);
        if (rest < 0) rest = 0;
    }

    public void hungerDepletionForHours(float numOfHours)
    {
        float realWorldTimescale = dayTime.getRealWorldTimescale();
        float secondsToAdvance = numOfHours * realWorldTimescale;
        hungerOverflowSeconds = secondsToAdvance - (hunger / (maxHunger / hungerSurvivalTime));
        //Negative overflow means we have time left... so just set it to 0.
        if (hungerOverflowSeconds < 0) hungerOverflowSeconds = 0;
        hunger = hunger - ((maxHunger / hungerSurvivalTime) * secondsToAdvance);
        if (hunger < 0) hunger = 0;
    }

    public void thirstDepletionForHours(float numOfHours)
    {
        float realWorldTimescale = dayTime.getRealWorldTimescale();
        float secondsToAdvance = numOfHours * realWorldTimescale;
        thirstOverflowSeconds = secondsToAdvance - (thirst / (maxThirst / thirstSurvivalTime));
        //Negative overflow means we have time left... so just set it to 0.
        if (thirstOverflowSeconds < 0) thirstOverflowSeconds = 0;
        thirst = thirst - ((maxThirst / thirstSurvivalTime) * secondsToAdvance);
        if (thirst < 0) thirst = 0;
    }

    public void depleteEnergyFromOverflow()
    {
        if (hungerOverflowSeconds == 0 && thirstOverflowSeconds == 0) return;

        //Deplete the larger of the two... the one that's affected us the longest takes priority.
        if (hungerOverflowSeconds > thirstOverflowSeconds)
        {
            energy = energy - ((maxEnergy / energySurvivalTime) * hungerOverflowSeconds);
        } else {
            energy = energy - ((maxEnergy / energySurvivalTime) * thirstOverflowSeconds);
        } 


        if (energy < 0) energy = 0;

        //Reset for the next pass
        hungerOverflowSeconds = 0;
        thirstOverflowSeconds = 0;
    }

    public void replenishEnergyForHours(float numOfHours)
    {
        float realWorldTimescale = dayTime.getRealWorldTimescale();
        float secondsToAdvance = numOfHours * realWorldTimescale;

        //Check for overflow and deplete that time away so we don't restore energy we actually lost.
        if (hungerOverflowSeconds > 0 || thirstOverflowSeconds > 0)
        {
            if (hungerOverflowSeconds > thirstOverflowSeconds)
            {
                secondsToAdvance -= hungerOverflowSeconds;
            } else
            {
                secondsToAdvance -= thirstOverflowSeconds;
            }
        }

        energy = energy + ((maxEnergy / energySurvivalTime) * secondsToAdvance);
        if (energy > maxEnergy) energy = maxEnergy;
    }

    public string getSurvivalTime()
    {
        if (totalSurvivalTime < 1) return "The desert has claimed you! \nYou survived for less than an hour.";
        int days = (int)totalSurvivalTime / 24;
        int hours = (int)totalSurvivalTime - (days * 24);
        return "The desert has claimed you! \nYou survived for " + days + " days and " + hours + " hours.";
    }

    public void toggleSliders()
    {
        hungerSlider.gameObject.SetActive(!hungerSlider.gameObject.activeInHierarchy);
        hungerImage.gameObject.SetActive(!hungerImage.gameObject.activeInHierarchy);
        thirstSlider.gameObject.SetActive(!thirstSlider.gameObject.activeInHierarchy);
        thirstImage.gameObject.SetActive(!thirstImage.gameObject.activeInHierarchy);
        energySlider.gameObject.SetActive(!energySlider.gameObject.activeInHierarchy);
        energyImage.gameObject.SetActive(!energyImage.gameObject.activeInHierarchy);
        fatigueSlider.gameObject.SetActive(!fatigueSlider.gameObject.activeInHierarchy);
        fatigueImage.gameObject.SetActive(!fatigueImage.gameObject.activeInHierarchy);
    }
}
