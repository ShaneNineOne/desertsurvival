﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityStandardAssets.Characters.FirstPerson;
using AC.TimeOfDaySystemFree;
using UnityEngine;
using UnityEngine.UI;

public class playerInteractor : MonoBehaviour {

    private FirstPersonController playerControl;
    private TimeOfDayManager dayTimeManager;
    private Camera cameraPOV;

    public static playerInteractor self;

    /// <summary>
    /// Tracks time for use in cooldowns.
    /// </summary>
    [SerializeField] private float timer;
    /// <summary>
    /// The distance we check for interactableObjects.
    /// </summary>
    [SerializeField] private float detectionDistance;
    /// <summary>
    /// Have we detected an interactable object?
    /// </summary>
    [SerializeField] private bool objectDetected;
    /// <summary>
    /// Are we examining an interactable object?
    /// </summary>
    [SerializeField] private bool examiningObject;
    /// <summary>
    /// Should we allow the UI to function or not (in case we are in a cutscene, another UI menu, etc.)
    /// </summary>
    public bool enableInteractiveUI;
    /// <summary>
    /// The transform of the object we have detected.
    /// </summary>
    [SerializeField] private Transform detectedObject;
    /// <summary>
    /// The basic interactableObject class of the detectedObject
    /// </summary>
    [SerializeField] private interactableObject detectedObjectInterface;
    /// <summary>
    /// What type is the interactableObject?
    /// </summary>
    [SerializeField] private int detectedObjectType;

    //NOTE: JUST DRAG AND DROP THE RESPECTIVE INTERACTIVE OBJECT CANVAS ELEMENTS INTO HERE WHEN SETTING UP A NEW SCENE.
    [Header("Interactive Object UI")]
    public GameObject interactCanvas;
    public GameObject sleepUI;
    public Text objectNameText;
    public Text objectInteractText;

    // Use this for initialization
    void Start() {
        self = this;
        playerControl = gameObject.GetComponent<FirstPersonController>();
        dayTimeManager = GameObject.FindGameObjectWithTag("DayTimeManager").GetComponent<TimeOfDayManager>();
        cameraPOV = Camera.main;
    }

    private void Update()
    {
        if (!enableInteractiveUI) return;

        if (Input.GetMouseButtonUp(0) && objectDetected)
        {
            if (detectedObjectType == 0)
            {
                examiningObject = !examiningObject;
                interactCanvas.SetActive(!examiningObject);//Active if we are not examining, not active if we are
                playerControl.toggleCanMove();
                dayTimeManager.togglePlaytime();
            }
            else if (detectedObjectType == 1 && cooldownCheck())
            {
                SurvivalManager.survivalSettings.addHunger(detectedObject.GetComponent<foodObject>().getAmountRestored());
                if (detectedObjectInterface.getDestroyOnUse())
                {
                    if (detectedObject.GetComponent<foodObject>().getWasSpawned() && FoodSpawnManager.foodSpawner != null)
                    {
                        FoodSpawnManager.foodSpawner.currentNumOfFoods--;
                    }
                    destroyCurrentObject();
                }
                else
                {
                    detectedObjectInterface.setLastUse(timer);
                }
            }
            else if (detectedObjectType == 2 && cooldownCheck() && !playerControl.getIgnoreGravity())
            {
                SurvivalManager.survivalSettings.addThirst(detectedObject.transform.GetComponent<foodObject>().getAmountRestored());
                if (detectedObjectInterface.getDestroyOnUse())
                {
                    if (detectedObject.GetComponent<foodObject>().getWasSpawned() && FoodSpawnManager.foodSpawner != null)
                    {
                        FoodSpawnManager.foodSpawner.currentNumOfDrinks--;
                    }
                    destroyCurrentObject();
                }
                else
                {
                    detectedObjectInterface.setLastUse(timer);
                }
            }
            else if (detectedObjectType == 3)
            {
                
                //Freeze everything.
                playerControl.showCursor();
                enableInteractiveUI = false;
                hideInteractText();
                playerControl.setCanMove(false);
                dayTimeManager.playTime = false;
                SurvivalManager.survivalSettings.enableDecay = false;
                sleepUI.SetActive(true);
            }
        }
    }

    // Update is called once per frame
    void FixedUpdate() {


        if (!enableInteractiveUI) return;

        //ALWAYS maintain the timer while the game is in motion, it is used for cooldown checks.
        timer += Time.fixedDeltaTime;
        RaycastHit hitInfo;
        LayerMask mask = (1 << 9 | 1 << 4);
        if (Physics.Raycast(cameraPOV.transform.position, cameraPOV.transform.forward, out hitInfo, detectionDistance, mask))
        {
            //Don't go through the setup if we're still looking at the same thing
            if (detectedObject != hitInfo.transform)
            {
                objectDetected = true;
                detectedObject = hitInfo.transform;
                detectedObjectInterface = detectedObject.GetComponent<interactableObject>();
                detectedObjectType = hitInfo.transform.GetComponent<interactableObject>().getObjectType();
                showInteractText();
            }
        } else
        {
            objectDetected = false;
            detectedObject = null;
            detectedObjectType = -1;
            interactCanvas.SetActive(false);
        }
    }

    public void setEnableInteractiveUI(bool setTo)
    {
        enableInteractiveUI = setTo;
    }

    public void hideInteractText()
    {
        interactCanvas.SetActive(false);
    }

    private void showInteractText()
    {
        if (examiningObject) return;

        if (detectedObjectType == 0)
        {
            objectInteractText.text = "Click to examine";
            objectNameText.text = detectedObject.GetComponent<interactableObject>().getObjectName();
        }
        else if (detectedObjectType == 1)
        {
            if (cooldownCheck())
            {
                objectInteractText.text = "Click to eat";
                objectNameText.text = detectedObject.GetComponent<interactableObject>().getObjectName();
            }
            else
            {
                objectInteractText.text = "Not harvestable yet...";
                objectNameText.text = detectedObject.GetComponent<interactableObject>().getObjectName();
            }
        }
        else if (detectedObjectType == 2 && !playerControl.getIgnoreGravity())
        {
            if (cooldownCheck())
            {
                objectInteractText.text = "Click to drink";
                objectNameText.text = detectedObject.GetComponent<interactableObject>().getObjectName();
            }
            else
            {
                objectInteractText.text = "Not drinkable yet...";
                objectNameText.text = detectedObject.GetComponent<interactableObject>().getObjectName();
            }
        } else if (detectedObjectType == 3)
        {
            objectInteractText.text = "Click to sleep";
            objectNameText.text = detectedObject.GetComponent<interactableObject>().getObjectName();
        }
        interactCanvas.SetActive(true);
    }

    private void destroyCurrentObject()
    {
        detectedObjectType = -1;
        objectDetected = false;
        Destroy(detectedObject.gameObject);
        detectedObject = null;
    }

    private bool cooldownCheck()
    {
        return timer >= (detectedObjectInterface.getCooldownTime() + detectedObjectInterface.getLastUse());
    }

    private void OnGUI()
    {
        if (examiningObject && enableInteractiveUI)
        {
            this.formNiceGUIDescriptionFieldFrom(detectedObject.GetComponent<interactableObject>().getObjectText());
        }
    }

    private void formNiceGUIDescriptionFieldFrom(string outputString)
    {
        float width = 400.0f;
        GUIStyle style = GUI.skin.box;
        GUIContent content = new GUIContent(outputString);

        style.alignment = TextAnchor.UpperLeft;
        style.wordWrap = true;

        //Determine the height that we will need to fit all of the text within our textbox.      
        float height = style.CalcHeight(content, width);

        float offsetY = 40f;
        //Run math to center the text box and account for the Y offset
        GUI.Box(new Rect(Screen.width / 2 - (width / 2), Screen.height / 2 - (0.5f * (2.0f * height) + offsetY), width, 2.0f * height), outputString);

    }
}
