﻿using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.Characters.FirstPerson;
using AC.TimeOfDaySystemFree;
using UnityEngine;
using UnityEngine.UI;

public class SleepManager : MonoBehaviour {

    [SerializeField] private FirstPersonController playerControl;
    [SerializeField] private playerInteractor playerInteractionControl;
    [SerializeField] private GameObject sleepUIPanel;

    public Text hoursText;
    private byte hoursIndex = 1;
    /// <summary>
    /// The old day in seconds timescale, used when fast forwarding time.
    /// </summary>
    private float oldDayInSeconds;
    /// <summary>
    /// The number of hours advanced when fast forwarding time.
    /// </summary>
    private float hoursAdvanced;

    [SerializeField] private bool isShowingUI;

    public void setIsShowing(bool setTo)
    {
        isShowingUI = setTo;
    }
    
    public void hide()
    {
        StartCoroutine(hideAtFrameEnd());
    }

    private IEnumerator hideAtFrameEnd()
    {
        //NOTE: We wait until the end of the frame before executing this to ensure our click doesn't "bleed over" into playerInteractor, creating a loop of reopening the sleep UI in the process.
        yield return new WaitForEndOfFrame();
        sleepUIPanel.SetActive(false);
        hoursIndex = 1;
        hoursText.text = "1 Hour";
        playerControl.setCanMove(true);
        playerControl.hideCursor();
        TimeOfDayManager.self.playTime = true;
        SurvivalManager.survivalSettings.enableDecay = true;
        setIsShowing(false);
        playerInteractionControl.setEnableInteractiveUI(true);
    }

    public void next()
    {
        hoursIndex++;
        if (hoursIndex > 24) hoursIndex = 24;
        hoursText.text = formatHoursText();
    }

    public void previous()
    {
        hoursIndex--;
        if (hoursIndex < 1) hoursIndex = 1;
        hoursText.text = formatHoursText();
    }

    public void sleep()
    {
        sleepUIPanel.SetActive(false);

        //Spawn new foods if we have to.
        if (FoodSpawnManager.foodSpawner != null)
        {
            FoodSpawnManager.foodSpawner.spawnFoods();
            FoodSpawnManager.foodSpawner.spawnDrinks();
        }

        oldDayInSeconds = TimeOfDayManager.self.dayInSeconds;
        TimeOfDayManager.self.playTime = true;
        OverheadCamera.overheadCameraManager.enableOverheadCamera();
        PlayerVisualController.playerVisual.forceEffects = true;
        PlayerVisualController.playerVisual.forceDefaultEffects();
        SurvivalManager.survivalSettings.showGUI = false;
        StartCoroutine(sleepRoutine());
    }

    private IEnumerator sleepRoutine()
    {
        //Speed up time of day flow + restore it so we can speed along.
        TimeOfDayManager.self.dayInSeconds = Mathf.Lerp(oldDayInSeconds, 20f, 10f);

        while (hoursAdvanced < (float)hoursIndex)
        {
            hoursAdvanced += TimeOfDayManager.self.getTimeAdvanced();
            yield return null;
        }

        OverheadCamera.overheadCameraManager.disableOverheadCamera();

        //Reset time of day to old scale + reset internal values.
        hoursAdvanced = 0;
        TimeOfDayManager.self.playTime = false;
        TimeOfDayManager.self.dayInSeconds = oldDayInSeconds;
        TimeOfDayManager.self.playTime = true;

        //Hunger + Thirst + Energy Depletions
        SurvivalManager.survivalSettings.hungerDepletionForHours((float)hoursIndex);
        SurvivalManager.survivalSettings.thirstDepletionForHours((float)hoursIndex);
        SurvivalManager.survivalSettings.restRegenerationForHours((float)hoursIndex);
        SurvivalManager.survivalSettings.replenishEnergyForHours((float) hoursIndex);
        SurvivalManager.survivalSettings.depleteEnergyFromOverflow();

        //Visual effect reset to current effect based on survival settings
        PlayerVisualController.playerVisual.setCurrentState();
        PlayerVisualController.playerVisual.forceCurrentEffects();
        PlayerVisualController.playerVisual.forceEffects = false;

        //Reset everything for next use + restore player control.
        SurvivalManager.survivalSettings.showGUI = true;
        hoursIndex = 1;
        hoursText.text = "1 Hour";
        playerControl.setCanMove(true);
        playerControl.hideCursor();
        TimeOfDayManager.self.playTime = true;
        SurvivalManager.survivalSettings.enableDecay = true;
        setIsShowing(false);
        playerInteractionControl.setEnableInteractiveUI(true);
    }

    private string formatHoursText()
    {
        if (hoursIndex == 1) return "1 Hour";
        else return hoursIndex + " Hours";
    }
}
